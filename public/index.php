<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
    if (!empty($_GET['save'])) {
      // Если есть параметр save, то выводим сообщение пользователю.
      print('Спасибо, результаты сохранены.');
    }
    // Включаем содержимое файла form.php.
    include('form.php');
    // Завершаем работу скрипта.
    exit();
  }

  $errors = FALSE;
  if (empty($_POST['field-name'])) {
    print('Заполните имя.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['field-email'])) {
    print('Заполните e-mail.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['field-date'])) {
    print('Укажите дату.<br/>');
    $errors = TRUE;
  }

//   if (empty($_POST['radio-sex'])) {
//     print('Укажите пол.<br/>');
//     $errors = TRUE;
//   }

//   if (empty($_POST['radio-limb'])) {
//     print('Укажите кол-во конечностей.<br/>');
//     $errors = TRUE;
//   }

  if (empty($_POST['superpower'])) {
    print('Укажите свои суперспособности.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['BIO'])) {
    print('Заполните биографию.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['ch'])) {
    print('Нажмите чек-бокс.<br/>');
    $errors = TRUE;
  }

  if ($errors) {
    // При наличии ошибок завершаем работу скрипта.
    exit();
  }


$name = $_POST['field-name'];
$email = $_POST['field-email'];
$bday = $_POST['field-date'];
$sex = $_POST['radio-sex'];
$limbs = $_POST['radio-limb'];
$super = $_POST['superpower'];
$bio = $_POST['BIO'];
$che = $_POST['ch'];

$db_host = "localhost"; 
$db_user = "u20325"; // Логин БД
$db_password = "1210789"; // Пароль БД
$db_base = 'u20325'; // Имя БД
$db_table = "form"; // Имя Таблицы БД

$result;

try {
    // Подключение к базе данных
    $db = new PDO("mysql:host=$db_host;dbname=$db_base", $db_user, $db_password);
    // Устанавливаем корректную кодировку
    $db->exec("set names utf8");
    // Собираем данные для запроса
    $data = array( 'name' => $name, 'email' => $email, 'bday' => $bday, 'sex' => $sex, 'limbs' => $limbs, 'super' => $super, 'bio' => $bio, 'che' => $che);
    // Подготавливаем SQL-запрос
    $query = $db->prepare("INSERT INTO $db_table (name, email, bday, sex, limbs, super, bio, che) values (:name, :email, :bday, :sex, :limbs, :super, :bio, :che)");
    // Выполняем запрос с данными
    $query->execute($data);
    // Запишим в переменую, что запрос отрабтал
    $result = true;
} catch (PDOException $e) {
    // Если есть ошибка соединения, выводим её
    print "Ошибка!: " . $e->getMessage() . "<br/>";
}


if ($result) {
    echo "Информация занесена в базу данных";
  }

?>

